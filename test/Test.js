const expect = require("chai").expect;
const request = require('request');
const versionShould = '0.0.1';
const versionIs = require('../package.json').version;

describe("versionTest", function() {

  it("returns package.json version", function(done) {


      expect(versionIs).to.equal(versionShould);
      done();
  });
});
